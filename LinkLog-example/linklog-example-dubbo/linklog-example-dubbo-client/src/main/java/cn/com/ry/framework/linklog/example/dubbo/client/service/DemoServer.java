package cn.com.ry.framework.linklog.example.dubbo.client.service;

import java.util.List;

public interface DemoServer {
    List<String> sayHello(String name);
}
