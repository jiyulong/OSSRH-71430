package cn.com.ry.framework.linklog.example.dubbo.client;

import cn.com.ry.framework.linklog.example.dubbo.client.service.DemoServer;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class Consumer {
    public static void main(String[] args) {
        //测试常规服务
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("spring-dubbo.xml");
        context.start();
        System.err.println("consumer start");
        DemoServer demoService = (DemoServer) context.getBean("demoServer");
        System.err.println("consumer");
        List<String> sayList = demoService.sayHello("reywong");
        if (sayList != null) {
            for (String say : sayList) {
                System.out.println(say);
            }
        }

    }
}
