package cn.com.ry.framework.linklog.example.dubbo.client.controller;


import cn.com.ry.framework.linklog.example.dubbo.server.service.DemoServer;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping("")
public class DemoController {
    private static final Logger logger = LoggerFactory.getLogger(DemoController.class);

    @Autowired
    DemoServer demoServer;

    @RequestMapping("/index")
    public String fsp(HttpServletRequest request, HttpServletResponse response) {
        StringBuilder stringBuilder = new StringBuilder();
        logger.info("1.请求参数{}", "reywong");
        List<String> sayList = demoServer.sayHello("reywong");
        if (sayList != null) {
            for (String say : sayList) {
                stringBuilder.append(say);
            }
        }
        String result = stringBuilder.toString();
        logger.info(result);

        logger.info("2.请求参数{}", "reywong");
        demoServer.sayHello("reywong");

        logger.info("3.httpclient 请求接口");
        httpClient();

        return result;
    }


    @RequestMapping("/httpclient")
    public String httpClient() {
        String msg = "";
        // 获得Http客户端(可以理解为:你得先有一个浏览器;注意:实际上HttpClient与浏览器是不一样的)
        CloseableHttpClient httpClient = HttpClientBuilder
                .create()
                .build();


        // 创建Get请求
        HttpGet httpGet = new HttpGet("http://localhost:8082/springMvc");
        // 响应模型
        CloseableHttpResponse response = null;
        try {
            Thread.sleep(1000);
            // 由客户端执行(发送)Get请求
            logger.info("request springMVC");
            response = httpClient.execute(httpGet);
            // 从响应模型中获取响应实体
            HttpEntity responseEntity = response.getEntity();
            msg = responseEntity.toString();
            if (responseEntity != null) {
                logger.info("reponse get springMVC");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                // 释放资源
                if (httpClient != null) {
                    httpClient.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return msg;
    }
}
