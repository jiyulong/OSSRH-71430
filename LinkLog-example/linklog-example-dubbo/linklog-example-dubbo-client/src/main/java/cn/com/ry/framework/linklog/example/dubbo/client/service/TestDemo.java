package cn.com.ry.framework.linklog.example.dubbo.client.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class TestDemo implements ApplicationContextAware, InitializingBean {
    public ApplicationContext applicationContext;

    @Override
    public void afterPropertiesSet() {
        try {
            TestDemoBean testDemoBean = (TestDemoBean) applicationContext.getBean("testDemoBean");
            testDemoBean.setName("reywong");

        } catch (Exception e) {

        }

        System.out.println("=============");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
