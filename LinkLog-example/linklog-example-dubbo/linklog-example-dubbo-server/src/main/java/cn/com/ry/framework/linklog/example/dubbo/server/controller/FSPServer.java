package cn.com.ry.framework.linklog.example.dubbo.server.controller;


import cn.com.ry.framework.linklog.httpclient.filter.LinkLogHttpRequestInterceptor;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("")
public class FSPServer {
    private static final Logger logger = LoggerFactory.getLogger(FSPServer.class);

    @RequestMapping("/springMvc")
    public String springMvc(HttpServletRequest request, HttpServletResponse response) {
        logger.info("FSP-Server springMvc");
//        httpClient();
        return "springMvc";
    }

    public String httpClient() {
        String msg = "";
        // 获得Http客户端(可以理解为:你得先有一个浏览器;注意:实际上HttpClient与浏览器是不一样的)
        CloseableHttpClient httpClient = HttpClientBuilder
                .create()
                .addInterceptorFirst(new LinkLogHttpRequestInterceptor())
                .build();


        // 创建Get请求
        HttpGet httpGet = new HttpGet("http://localhost:8081/httpclient");
        // 响应模型
        CloseableHttpResponse response = null;
        try {
            Thread.sleep(1000);
            // 由客户端执行(发送)Get请求
            logger.info("request springMVC");
            response = httpClient.execute(httpGet);
            // 从响应模型中获取响应实体
            HttpEntity responseEntity = response.getEntity();
            msg = responseEntity.toString();
            if (responseEntity != null) {
                logger.info("reponse get springMVC");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                // 释放资源
                if (httpClient != null) {
                    httpClient.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return msg;
    }
}
