package cn.com.ry.framework.linklog.example.dubbo.server.service.impl;

import cn.com.ry.framework.linklog.example.dubbo.server.service.DemoServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class DemoServerImpl implements DemoServer {
    private static final Logger logger = LoggerFactory.getLogger(DemoServer.class);

    @Override
    public List<String> sayHello(String name) {
        logger.info("请求参数：" + name);
        List<String> helloList = new ArrayList();
        helloList.add("Hello " + name);
        return helloList;
    }
}
