<?xml version="1.0" encoding="UTF-8"?>
<configuration scan="true" scanPeriod="10 seconds" debug="true">

    <jmxConfigurator/>
    <!--支持多线程MDC传递-->
    <contextListener class="com.ofpay.logback.TtlMdcListener"/>

    <!--start property-->
    <!--start 引入配置，获取app.name-->
    <property resource="META-INF/app.properties"/>
    <!--end 引入配置-->

    <!--start 日志文件参数-->
    <!--打印日志路径-->
    <property name="log_dir" value="/app/logs"/>
    <!--日志文件名-->
    <property name="fileName" value="server"/>
    <!--接口数据-->
    <property name="linkLogMonitorName" value="linklog_monitor"/>
    <!--错误日志文件名-->
    <property name="errorName" value="error"/>
    <!--单个文件大小-->
    <property name="maxFileSize" value="100MB"></property>
    <!--最小保留文件数-->
    <property name="minIndex" value="1"></property>
    <!--最大保留文件数-->
    <property name="maxIndex" value="10"></property>
    <!--end 日志文件参数-->

    <property name="env" value="${linklog.env:- sit}"></property>
    <!--start kafka参数-->
    <!-- 是否使用 kafka( linklog.kafka.json=kafkaJson 使用) -->
    <property name="kafkaJson" value="${linklog.kafka.json:- }"></property>
    <!--kafka地址-->
    <property name="bootstrapServers" value="${linklog.kafka.servers:-10.201.1.46:9092}"></property>
    <!--对应写入打的topic-->
    <property name="topic" value="${linklog.kafka.topic:-linklog_std_log}"></property>
    <!--错误日志topic-->
    <property name="errorTopic" value="${linklog.kafka.errortopic:-linklog_common_error}"></property>
    <!--不等kafka返回-->
    <property name="acks" value="0"></property>
    <!--等待最长1000毫秒并收集日志消息，然后将其作为批处理发送-->
    <property name="linger.ms" value="1000"></property>
    <!--即使producer缓冲区已满，也不要阻止应用程序，而是开始丢弃消息-->
    <property name="max.block.ms" value="0"></property>
    <!--end kafka参数-->

    <!--start 异步的参数-->
    <!--queueSize 队列深度(影响系统性能)，默认值256，不过该值首次建议设置大一些，后续根据自己业务的特点去调优。-->
    <property name="discardingThreshold" value="0"></property>
    <!--discardingThreshold：默认情况下，当blockingQueue的容量高于阈值时（80%），会丢弃ERROR一下级别的日志(那可以极大的提升性能)，如果不希望丢弃日志那可以设置为0。-->
    <property name="queueSize" value="1024"></property>
    <!--提取调用者数据的代价是相当昂贵的。为了提升性能，默认情况下，当event被加入到queue时，event关联的调用者数据不会被提取。默认情况下，只有"cheap"的数据，如线程名。-->
    <property name="includeCallerData" value="true"></property>
    <!--end 异步的参数-->
    <!--start 日志打印格式-->
    <property name="log.pattern"
              value="[%X{linkLog}][%date{yyyy-MM-dd HH:mm:ss.SSS}] [%thread] [%-5level] [%logger:%line] --%mdc{client} %msg%n"/>
    <!--
                date: 日志打印时间
                ip : 主机ip java.rmi.server.hostname
                env: 应用环境 test | sit | pre | pro
                appname:应用名称
                logtype: 日志类型  LinkLogTrace | LinkLogMonitor | LinkLogData
                level: 日志等级
                linkLog: linkLog提供的功能
                thread: 线程名称
                class: 输出Logger 的名字
                method:方法名称
                line: 行数
                message: 内容
                stack_trace "%exception{5}",
                -->
    <!--消息为文本-->

    <property name="messageText" value='{
    "date":"%date{yyyy-MM-dd&apos;T&apos;HH:mm:ss.SSSZ}",
    "ip": "${java.rmi.server.hostname:-127.0.0.1}",
    "env":"${env}",
    "appname": "${app.name:-.}",
    "logtype":"${logtype:-LinkLogTrace}",
    "level": "%-5level",
    "linkLog":"#asJson{%X{linkLog}}",
    "thread": "%thread",
    "engineRole":"${engine.role:-.}",
    "class": "%logger",
    "method": "%M",
    "line": "%L",
    "message": "%m",
    "stack_trace": "%exception"
    }'></property>

    <!--end 日志打印格式-->
    <!--end property-->

    <!--start appender-->
    <!--精简的格式-->
    <appender name="messageText_console" class="ch.qos.logback.core.ConsoleAppender">
        <encoder>
            <pattern>${log.pattern}</pattern>
            <charset>UTF-8</charset>
        </encoder>
    </appender>
    <!--json格式日志，日志内容为text-->
    <appender name="kafkaJsonmessageText_console" class="ch.qos.logback.core.ConsoleAppender">
        <encoder class="net.logstash.logback.encoder.LoggingEventCompositeJsonEncoder">
            <providers>
                <pattern>
                    <pattern>
                        ${messageText}
                    </pattern>
                </pattern>
            </providers>
        </encoder>
    </appender>

    <appender name="messageText_rollingFile" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <encoder>
            <pattern>${log.pattern}</pattern>
        </encoder>
        <file>${log_dir}/${fileName}.log</file>
        <rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
            <fileNamePattern>${log_dir}/${fileName}-%i.log</fileNamePattern>
            <minIndex>${minIndex:-1}</minIndex>
            <maxIndex>${maxIndex:-10}</maxIndex>
        </rollingPolicy>
        <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
            <maxFileSize>${maxFileSize:-200MB}</maxFileSize>
        </triggeringPolicy>
    </appender>

    <appender name="kafkaJsonmessageText_rollingFile" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <encoder class="net.logstash.logback.encoder.LoggingEventCompositeJsonEncoder">
            <providers>
                <pattern>
                    <pattern>
                        ${messageText}
                    </pattern>
                </pattern>
            </providers>
        </encoder>
        <file>${log_dir}/linkLog-${fileName}.log</file>
        <rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
            <fileNamePattern>${log_dir}/linkLog-${fileName}-%i.log</fileNamePattern>
            <minIndex>${minIndex:-1}</minIndex>
            <maxIndex>${maxIndex:-10}</maxIndex>
        </rollingPolicy>
        <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
            <maxFileSize>${maxFileSize:-200MB}</maxFileSize>
        </triggeringPolicy>
    </appender>

    <appender name="messageText_error_rollingFile" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <encoder>
            <pattern>${log.pattern}</pattern>
            <charset>UTF-8</charset>
        </encoder>
        <file>${log_dir}/${errorName}.log</file>
        <!-- 日志级别过滤器 -->
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <!-- 过滤的级别 -->
            <level>ERROR</level>
            <!-- 匹配时的操作：接收（记录） -->
            <onMatch>ACCEPT</onMatch>
            <!-- 不匹配时的操作：拒绝（不记录） -->
            <onMismatch>DENY</onMismatch>
        </filter>
        <rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
            <fileNamePattern>${log_dir}/${errorName}-%i.log</fileNamePattern>
            <minIndex>${minIndex:-1}</minIndex>
            <maxIndex>${maxIndex:-10}</maxIndex>
        </rollingPolicy>
        <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
            <maxFileSize>${maxFileSize:-200MB}</maxFileSize>
        </triggeringPolicy>
    </appender>

    <appender name="kafkaJsonmessageText_error_rollingFile" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <encoder class="net.logstash.logback.encoder.LoggingEventCompositeJsonEncoder">
            <providers>
                <pattern>
                    <pattern>
                        ${messageText}
                    </pattern>
                </pattern>
            </providers>
        </encoder>
        <file>${log_dir}/linkLog-${errorName}.log</file>
        <!-- 日志级别过滤器 -->
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <!-- 过滤的级别 -->
            <level>ERROR</level>
            <!-- 匹配时的操作：接收（记录） -->
            <onMatch>ACCEPT</onMatch>
            <!-- 不匹配时的操作：拒绝（不记录） -->
            <onMismatch>DENY</onMismatch>
        </filter>
        <rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
            <fileNamePattern>${log_dir}/linkLog-${errorName}-%i.log</fileNamePattern>
            <minIndex>${minIndex:-1}</minIndex>
            <maxIndex>${maxIndex:-10}</maxIndex>
        </rollingPolicy>
        <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
            <maxFileSize>${maxFileSize:-200MB}</maxFileSize>
        </triggeringPolicy>
    </appender>

    <appender name="messageText_kafka" class="ch.qos.logback.core.ConsoleAppender">
        <encoder>
            <pattern>${log.pattern}</pattern>
            <charset>UTF-8</charset>
        </encoder>
        <!-- 日志级别过滤器 -->
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <!-- 过滤的级别 -->
            <level>ERROR</level>
            <!-- 匹配时的操作：接收（记录） -->
            <onMatch>ACCEPT</onMatch>
            <!-- 不匹配时的操作：拒绝（不记录） -->
            <onMismatch>DENY</onMismatch>
        </filter>
        <target>System.err</target>
    </appender>

    <appender name="kafkaJsonmessageText_kafka" class="com.github.danielwegener.logback.kafka.KafkaAppender">
        <encoder class="net.logstash.logback.encoder.LoggingEventCompositeJsonEncoder">
            <providers>
                <pattern>
                    <pattern>
                        ${messageText}
                    </pattern>
                </pattern>
            </providers>
        </encoder>
        <topic>${topic}</topic>
        <keyingStrategy class="com.github.danielwegener.logback.kafka.keying.NoKeyKeyingStrategy"/>
        <deliveryStrategy class="com.github.danielwegener.logback.kafka.delivery.AsynchronousDeliveryStrategy"/>
        <producerConfig>acks=${acks}</producerConfig>
        <producerConfig>linger.ms=${linger.ms}</producerConfig>
        <producerConfig>max.block.ms=${max.block.ms}</producerConfig>
        <producerConfig>bootstrap.servers=${bootstrapServers}</producerConfig>
        <producerConfig>client.id=${java.rmi.server.hostname:-127.0.0.1}-linklog</producerConfig>
    </appender>

    <appender name="messageText_error_kafka" class="ch.qos.logback.core.ConsoleAppender">
        <encoder>
            <pattern>${log.pattern}</pattern>
            <charset>UTF-8</charset>
        </encoder>
        <!-- 日志级别过滤器 -->
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <!-- 过滤的级别 -->
            <level>ERROR</level>
            <!-- 匹配时的操作：接收（记录） -->
            <onMatch>ACCEPT</onMatch>
            <!-- 不匹配时的操作：拒绝（不记录） -->
            <onMismatch>DENY</onMismatch>
        </filter>
        <target>System.err</target>
    </appender>

    <appender name="kafkaJsonmessageText_error_kafka" class="com.github.danielwegener.logback.kafka.KafkaAppender">
        <!-- 日志级别过滤器 -->
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <!-- 过滤的级别 -->
            <level>ERROR</level>
            <!-- 匹配时的操作：接收（记录） -->
            <onMatch>ACCEPT</onMatch>
            <!-- 不匹配时的操作：拒绝（不记录） -->
            <onMismatch>DENY</onMismatch>
        </filter>
        <encoder class="net.logstash.logback.encoder.LoggingEventCompositeJsonEncoder">
            <providers>
                <pattern>
                    <pattern>
                        ${messageText}
                    </pattern>
                </pattern>
            </providers>
        </encoder>
        <topic>${errorTopic}</topic>
        <keyingStrategy class="com.github.danielwegener.logback.kafka.keying.NoKeyKeyingStrategy"/>
        <deliveryStrategy class="com.github.danielwegener.logback.kafka.delivery.AsynchronousDeliveryStrategy"/>
        <producerConfig>acks=${acks}</producerConfig>
        <producerConfig>linger.ms=${linger.ms}</producerConfig>
        <producerConfig>max.block.ms=${max.block.ms}</producerConfig>
        <producerConfig>bootstrap.servers=${bootstrapServers}</producerConfig>
        <producerConfig>client.id=${java.rmi.server.hostname:-127.0.0.1}-linklog</producerConfig>
    </appender>

    <appender name="${kafkaJson}messageText_async_rollingFile" class="ch.qos.logback.classic.AsyncAppender">
        <discardingThreshold>${discardingThreshold}</discardingThreshold>
        <queueSize>${queueSize}</queueSize>
        <includeCallerData>${includeCallerData}</includeCallerData>
        <appender-ref ref="${kafkaJson}messageText_rollingFile"/>
    </appender>

    <appender name="${kafkaJson}messageText_error_async_rollingFile" class="ch.qos.logback.classic.AsyncAppender">
        <discardingThreshold>${discardingThreshold}</discardingThreshold>
        <queueSize>${queueSize}</queueSize>
        <includeCallerData>${includeCallerData}</includeCallerData>
        <appender-ref ref="${kafkaJson}messageText_error_rollingFile"/>
    </appender>


    <appender name="local_messageText_async_rollingFile" class="ch.qos.logback.classic.AsyncAppender">
        <discardingThreshold>${discardingThreshold}</discardingThreshold>
        <queueSize>${queueSize}</queueSize>
        <includeCallerData>${includeCallerData}</includeCallerData>
        <appender-ref ref="messageText_rollingFile"/>
    </appender>

    <appender name="local_messageText_error_async_rollingFile" class="ch.qos.logback.classic.AsyncAppender">
        <discardingThreshold>${discardingThreshold}</discardingThreshold>
        <queueSize>${queueSize}</queueSize>
        <includeCallerData>${includeCallerData}</includeCallerData>
        <appender-ref ref="messageText_error_rollingFile"/>
    </appender>

    <appender name="${kafkaJson}messageText_async_kafka" class="ch.qos.logback.classic.AsyncAppender">
        <discardingThreshold>${discardingThreshold}</discardingThreshold>
        <queueSize>${queueSize}</queueSize>
        <includeCallerData>${includeCallerData}</includeCallerData>
        <appender-ref ref="${kafkaJson}messageText_kafka"/>
    </appender>

    <appender name="${kafkaJson}messageText_error_async_kafka" class="ch.qos.logback.classic.AsyncAppender">
        <discardingThreshold>${discardingThreshold}</discardingThreshold>
        <queueSize>${queueSize}</queueSize>
        <includeCallerData>${includeCallerData}</includeCallerData>
        <appender-ref ref="${kafkaJson}messageText_error_kafka"/>
    </appender>

    <!--end appender-->

    <!--start logger-->
    <logger name="org.apache.zookeeper" level="ERROR"/>
    <logger name="org.apache.curator" level="OFF"/>
    <logger name="com.gillion.eds.zookeeper" level="ERROR"/>
    <logger name="RocketmqClient" level="ERROR"/>
    <logger name="RocketmqRemoting" level="ERROR"/>

    <!--打印sql-->
    <!--<logger name="cn.uce.crm.cmdb.dao" level="off" additivity="false">-->
    <!--<appender-ref ref="messageJson_console"/>-->
    <!--<appender-ref ref="messageText_async_kafka"/>-->
    <!--</logger>-->

    <root level="INFO">

        <!--控制台显示json格式，message为json-->
        <!--精简的格式-->
        <!--start 异步 message-text-->
        <appender-ref ref="messageText_console"/>
        <!--异步打印到文件，message为text-->
        <appender-ref ref="local_messageText_async_rollingFile"/>
        <!--异步打印到ERROR日志到文件，message为text-->
        <appender-ref ref="local_messageText_error_async_rollingFile"/>
        <!--异步打印到kafka json格式，message为text-->
        <appender-ref ref="${kafkaJson}messageText_async_kafka"/>
        <!--异步打印到ERROR日志到kafka  json，message为text-->
        <appender-ref ref="${kafkaJson}messageText_error_async_kafka"/>
        <!--end 异步json-text-->
    </root>
    <!--end logger-->
</configuration>
