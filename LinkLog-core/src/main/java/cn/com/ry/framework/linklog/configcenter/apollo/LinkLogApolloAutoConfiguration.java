package cn.com.ry.framework.linklog.configcenter.apollo;

import cn.com.ry.framework.linklog.configcenter.apollo.common.LinkLogApolloPluginCommon;
import cn.com.ry.framework.linklog.constant.LinkLogConstants;
import cn.com.ry.framework.linklog.util.LinkLogTypeUtil;
import cn.com.ry.framework.linklog.util.SystemAdapterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;


public class LinkLogApolloAutoConfiguration implements ApplicationContextInitializer {
    private static final Logger logger = LoggerFactory.getLogger(LinkLogApolloAutoConfiguration.class);

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        SystemAdapterUtil.checkSystemType();
        //自动加载Apollo
        if (LinkLogTypeUtil.checkLinkLogType(LinkLogConstants.LINKLOG_APOLLO)) {
            logger.info("[linkLog][开始]加载LinkLogApolloPlugin");
            LinkLogApolloPluginCommon.setLinkLogApolloPlugin((AbstractApplicationContext) applicationContext);
            logger.info("[linkLog][结束]加载LinkLogApolloPlugin");
        }

    }
}
