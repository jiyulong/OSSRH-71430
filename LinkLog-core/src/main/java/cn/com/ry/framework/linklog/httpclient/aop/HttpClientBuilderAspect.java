package cn.com.ry.framework.linklog.httpclient.aop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 自定义埋点注解切面，用于拦截httpClient注入LinkLogHttpRequestInterceptor
 */
//@Aspect
public class HttpClientBuilderAspect {
    private static final Logger logger = LoggerFactory.getLogger(HttpClientBuilderAspect.class);

//    @Pointcut("call(public * org.apache.http.impl.client.HttpClientBuilder.build())")
//    public void pointCut() {
//    }
//
//    @Before("pointCut()")
//    public void before(JoinPoint jpJoinPoint) {
//        logger.info("[start]执行org.apache.http.impl.client.HttpClientBuilder.build()切面");
//        ((HttpClientBuilder) jpJoinPoint.getThis()).addInterceptorFirst(new LinkLogHttpRequestInterceptor());
//        logger.info("[end]执行org.apache.http.impl.client.HttpClientBuilder.build()切面");
//
//    }
}
