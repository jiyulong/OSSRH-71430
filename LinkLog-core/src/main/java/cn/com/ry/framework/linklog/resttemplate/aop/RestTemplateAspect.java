package cn.com.ry.framework.linklog.resttemplate.aop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 自定义埋点注解切面，用于拦截httpClient注入LinkLogHttpRequestInterceptor
 */
//@Aspect
public class RestTemplateAspect {
    private static final Logger logger = LoggerFactory.getLogger(RestTemplateAspect.class);

//    @Pointcut("call(public * org.springframework.web.client.RestTemplate.RestTemplate(..))")
//    public void restTemplatePointCut() {
//    }
//
//    @After("restTemplatePointCut()")
//    public void after(JoinPoint jpJoinPoint) {
//        logger.info("[start]执行org.springframework.web.client.RestTemplate.RestTemplate(..)切面");
//        ((RestTemplate) jpJoinPoint.getThis()).getInterceptors().add(new LinkLogRestTemplateFilter());
//        logger.info("[end]执行org.springframework.web.client.RestTemplate.RestTemplate(..)切面");
//
//    }
}
