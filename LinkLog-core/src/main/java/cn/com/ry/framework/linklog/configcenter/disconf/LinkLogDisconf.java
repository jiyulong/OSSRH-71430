package cn.com.ry.framework.linklog.configcenter.disconf;

import cn.com.ry.framework.linklog.constant.LinkLogConstants;
import cn.com.ry.framework.linklog.logging.LinkLogSystem;
import cn.com.ry.framework.linklog.util.LinkLogSwitch;
import com.baidu.disconf.client.common.annotations.DisconfUpdateService;
import com.baidu.disconf.client.common.update.IDisconfUpdate;
import com.baidu.disconf.client.store.DisconfStoreProcessor;
import com.baidu.disconf.client.store.DisconfStoreProcessorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.Set;


@DisconfUpdateService(confFileKeys = {"linklog.properties"})
public class LinkLogDisconf implements IDisconfUpdate, ILinkLogDisconf {


    private static final Logger logger = LoggerFactory.getLogger(LinkLogDisconf.class);

    private static volatile Properties configProps = new Properties();

    private static final String accessLogKey = "linklog.accesslog";


    public void initDisconf() {
        InputStream inputStream = null;
        try {
            DisconfStoreProcessor disconfStoreProcessor = DisconfStoreProcessorFactory.getDisconfStoreFileProcessor();
            Set<String> fileSets = disconfStoreProcessor.getConfKeySet();
            if (fileSets != null && fileSets.size() > 0) {
                //加载文件
                for (String fileSet : fileSets) {
                    if (fileSet != null && fileSet.endsWith(LinkLogConstants.LINKLOG_SUFFIX)) {
                        inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileSet);
                        configProps.load(inputStream);
                        for (Map.Entry<Object, Object> entry : configProps.entrySet()) {
                            onDisconfItemChanged(fileSet, entry.getKey().toString(), entry.getValue());
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Disconf配置文件, 初次加载失败", e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {

                }
            }
        }
    }

    private void onDisconfItemChanged(String file, String key, Object value) {
        logger.info("Disconf配置文件项目变更, file={}, key={}, value={}", file, key, value);

        try {
            //修改accesslog日志状态
            if (key != null && value != null && key.equalsIgnoreCase(accessLogKey)) {
                if (String.valueOf(value).equalsIgnoreCase("true")) {
                    LinkLogSwitch.setACCESSLOG(true);
                } else if (String.valueOf(value).equalsIgnoreCase("false")) {
                    LinkLogSwitch.setACCESSLOG(false);
                }
            }
            //修改日志级别
            if (value != null) {
                LinkLogSystem.setLevel(key, value.toString());
            }
        } catch (Exception e) {
            logger.error("Disconf配置文件项目变更, 执行操作失败: ", e);
        }
    }


    @Override
    public void reload() {
        initDisconf();
    }
}
