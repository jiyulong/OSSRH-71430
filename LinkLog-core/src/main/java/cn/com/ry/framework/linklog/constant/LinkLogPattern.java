package cn.com.ry.framework.linklog.constant;

public class LinkLogPattern {
    private String traceId;
    private String spanId;
    private String preApp;
    private String preIp;
    private String preHost;

    public LinkLogPattern(String traceId, String spanId, String preApp, String preHost, String preIp) {
        this.traceId = traceId;
        this.spanId = spanId;
        this.preApp = preApp;
        this.preHost = preHost;
        this.preIp = preIp;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getSpanId() {
        return spanId;
    }

    public void setSpanId(String spanId) {
        this.spanId = spanId;
    }

    public String getPreApp() {
        return preApp;
    }

    public void setPreApp(String preApp) {
        this.preApp = preApp;
    }

    public String getPreIp() {
        return preIp;
    }

    public void setPreIp(String preIp) {
        this.preIp = preIp;
    }

    public String getPreHost() {
        return preHost;
    }

    public void setPreHost(String preHost) {
        this.preHost = preHost;
    }
}
