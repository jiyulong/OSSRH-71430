package cn.com.ry.framework.linklog.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

public class FileUtil {


    String getProperty(String name) {
        InputStream inputStream = null;
        try {
            String file = "/META-INF/app.properties";
            URL fileURL = this.getClass().getResource(file);
            Properties props = new Properties();
            if (fileURL != null) {
                inputStream = this.getClass().getResourceAsStream(file);
                props.load(inputStream);
            }
            return props.getProperty(name);
        } catch (Exception e) {

        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {

                }
            }
        }
        return null;
    }


    public String getPackName() {
        try {
            Package pack = getClass().getPackage();
            String packName = pack.getName();
            if (packName != null) {
                do {
                    packName = packName.substring(0, packName.lastIndexOf("."));
                    pack = Package.getPackage(packName);
                } while (null != pack);
            }

            return packName;
        } catch (Exception e) {
        }
        return null;

    }
}
