package cn.com.ry.framework.linklog.starter;

import cn.com.ry.framework.linklog.fegin.filter.LinkLogFeignFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Feign 配置的springboot自动装配类
 */
@Configuration
@ConditionalOnClass(name = {"feign.RequestInterceptor"})
public class LinkLogFeignAutoConfiguration {

    @Bean
    public LinkLogFeignFilter linkLogFeignFilter() {
        return new LinkLogFeignFilter();
    }
}
