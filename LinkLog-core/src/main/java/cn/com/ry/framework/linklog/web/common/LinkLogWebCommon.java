package cn.com.ry.framework.linklog.web.common;

import cn.com.ry.framework.linklog.constant.LinkLogConstants;
import cn.com.ry.framework.linklog.core.LinkLogHandler;
import cn.com.ry.framework.linklog.core.LinkLogLabel;
import cn.com.ry.framework.linklog.util.LinkLogPrintUtil;
import cn.com.ry.framework.linklog.util.LinkLogSwitch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * web这块的逻辑封装类
 */
public class LinkLogWebCommon extends LinkLogHandler {

    private final static Logger log = LoggerFactory.getLogger(LinkLogWebCommon.class);

    private static volatile LinkLogWebCommon linkLogWebCommon;

    public static LinkLogWebCommon loadInstance() {
        if (linkLogWebCommon == null) {
            synchronized (LinkLogWebCommon.class) {
                if (linkLogWebCommon == null) {
                    linkLogWebCommon = new LinkLogWebCommon();
                }
            }
        }
        return linkLogWebCommon;
    }

    public void preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String traceId = request.getHeader(LinkLogConstants.LINKLOG_TRACE_KEY);
        String spanId = request.getHeader(LinkLogConstants.LINKLOG_SPANID_KEY);
        String preApp = request.getHeader(LinkLogConstants.LINKLOG_PRE_APP_KEY);
        String preHost = request.getHeader(LinkLogConstants.LINKLOG_PRE_HOST_KEY);
        String preIp = request.getHeader(LinkLogConstants.LINKLOG_PRE_IP_KEY);

        LinkLogLabel labelBean = new LinkLogLabel(preApp, preHost, preIp, traceId, spanId);

        processProviderSide(labelBean);
        //打印日志
        if (LinkLogSwitch.ACCESSLOG) {
            buildAccessLog(request);
        }
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler) {
        cleanThreadLocal();
    }

    public void buildAccessLog(HttpServletRequest request) {
        String requestURI = "";
        String requestURLStr = "";
        String method = "";
        String queryString = "";
        String userAgent = "";
        String forwarded = "";
        queryString = request.getQueryString();
        requestURI = request.getRequestURI();
        StringBuffer requestURL = request.getRequestURL();
        if (requestURL != null) {
            requestURLStr = requestURL.toString();
        }
        method = request.getMethod();

        userAgent = request.getHeader("User-Agent");
        forwarded = request.getHeader("X-Forwarded-For");

        Map map = new HashMap();
        map.put("linkLogAccessType", LinkLogConstants.LINKLOG_LOG_SERVER);
        map.put("requestURI", requestURI);
        map.put("requestURLStr", requestURLStr);
        map.put("method", method);
        map.put("userAgent", userAgent);
        map.put("forwarded", forwarded);

        LinkLogPrintUtil.printRequestLog(queryString, LinkLogWebCommon.class.getName(), map);

    }

}
