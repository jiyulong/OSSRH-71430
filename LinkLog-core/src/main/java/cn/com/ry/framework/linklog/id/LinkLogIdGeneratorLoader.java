package cn.com.ry.framework.linklog.id;

public class LinkLogIdGeneratorLoader {

    private static LinkLogIdGenerator idGenerator = new LinkLogDefaultIdGenerator();

    public static LinkLogIdGenerator getIdGenerator() {
        return idGenerator;
    }

    public static void setIdGenerator(LinkLogIdGenerator idGenerator) {
        LinkLogIdGeneratorLoader.idGenerator = idGenerator;
    }
}
