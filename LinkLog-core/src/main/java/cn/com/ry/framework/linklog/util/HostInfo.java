package cn.com.ry.framework.linklog.util;

import cn.com.ry.framework.linklog.constant.LinkLogConstants;

import java.net.InetAddress;


public class HostInfo {
    private static String appName = initAppName();
    private static String ip = System.getProperty("java.rmi.server.hostname")==null?LinkLogConstants.UNKNOWN:System.getProperty("java.rmi.server.hostname");
    private static String hostName = LinkLogConstants.UNKNOWN;


    //初始化APPName
    public static String initAppName() {
        String initAppName = "";
        //1.判断jvm中是否存在
        initAppName=System.getProperty("app.name");
        if(initAppName!=null){
            return initAppName;
        }

        //2.判断META-INF/app.properties中是否存在
        FileUtil fileUtil=new FileUtil();
        initAppName=fileUtil.getProperty("app.name");
        if(initAppName!=null){
            return initAppName;
        }
        //3.获取包名
        initAppName=fileUtil.getPackName();
        if(initAppName!=null){
            return initAppName;
        }

        if(initAppName==null){
            initAppName=LinkLogConstants.UNKNOWN;
        }
        return initAppName;
    }

    public static String getAppName() {
        return appName;
    }

    public static void setAppName(String appName) {
        HostInfo.appName = appName;
    }

    public static String getIp() {
        return ip;
    }

    public static void setIp(String ip) {
        HostInfo.ip = ip;
    }

    public static String getHostName() {
        try {
            hostName = InetAddress.getLocalHost().getHostName();
        } catch (Exception e) {
        }
        return hostName;
    }

    public static void setHostName(String hostName) {
        HostInfo.hostName = hostName;
    }
}
