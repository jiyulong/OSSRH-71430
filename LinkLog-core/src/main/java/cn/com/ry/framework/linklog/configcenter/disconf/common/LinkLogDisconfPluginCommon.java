package cn.com.ry.framework.linklog.configcenter.disconf.common;

import cn.com.ry.framework.linklog.configcenter.disconf.ILinkLogDisconf;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.support.AbstractApplicationContext;


public class LinkLogDisconfPluginCommon {

    public static void setLinkLogDisconfPlugin(AbstractApplicationContext applicationContext) {
        if (applicationContext != null) {
            createLinkLogDisconf(applicationContext);
        }
    }


    public static void createLinkLogDisconf(AbstractApplicationContext applicationContext) {
        try {
            applicationContext.getBean("linkLogDisconf");
        } catch (Exception e) {
            try {
                BeanDefinitionBuilder linkLogDisconfBuilder = BeanDefinitionBuilder.genericBeanDefinition("cn.com.ry.framework.linklog.configcenter.disconf.LinkLogDisconf");


                linkLogDisconfBuilder.setScope("singleton");
                DefaultListableBeanFactory linkLogDisconfBeanFacotry = (DefaultListableBeanFactory) applicationContext.getBeanFactory();
                linkLogDisconfBeanFacotry.registerBeanDefinition("linkLogDisconf", linkLogDisconfBuilder.getBeanDefinition());
                //加载日志
                ILinkLogDisconf linkLogDisconf = (ILinkLogDisconf) applicationContext.getBean("linkLogDisconf");
                linkLogDisconf.initDisconf();
            } catch (Exception e1) {

            }

        }
    }

}
