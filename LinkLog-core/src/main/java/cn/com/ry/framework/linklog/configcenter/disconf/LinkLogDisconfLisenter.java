package cn.com.ry.framework.linklog.configcenter.disconf;

import cn.com.ry.framework.linklog.configcenter.disconf.common.LinkLogDisconfPluginCommon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class LinkLogDisconfLisenter implements ServletContextListener {
    private static final Logger logger = LoggerFactory.getLogger(LinkLogDisconfLisenter.class);


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        if (servletContextEvent != null) {
            AbstractApplicationContext applicationContext = (AbstractApplicationContext) WebApplicationContextUtils.getWebApplicationContext(servletContextEvent.getServletContext());
            logger.info("[linkLog][开始]加载LinkLogDisconfPlugin");
            LinkLogDisconfPluginCommon.setLinkLogDisconfPlugin(applicationContext);
            logger.info("[linkLog][结束]加载LinkLogDisconfPlugin");
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }

}
