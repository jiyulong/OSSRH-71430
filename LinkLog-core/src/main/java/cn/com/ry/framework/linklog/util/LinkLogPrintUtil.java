package cn.com.ry.framework.linklog.util;

import cn.com.ry.framework.linklog.constant.LinkLogConstants;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class LinkLogPrintUtil {
    private final static Logger logger = LoggerFactory.getLogger("linkLog");

    public static void printRequestLog(String content, String filterName, Map map) {
        try {
            if (content != null && content.length() > LinkLogConstants.maxContentLength) {
                map.put("content", "内容打印1M,不打印");
            } else {
                map.put("content", content);
            }
            map.put("filterName", filterName);
            logger.info(JSON.toJSONString(map));

        } catch (Exception e) {
            logger.error("请求日志打印异常", e);
        }

    }
}
