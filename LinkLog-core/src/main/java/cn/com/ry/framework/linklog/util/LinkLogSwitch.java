package cn.com.ry.framework.linklog.util;

public class LinkLogSwitch {
    //是否打印请求日志 false 关闭 true 开启
    public static Boolean ACCESSLOG = true;

    public static Boolean getACCESSLOG() {
        return ACCESSLOG;
    }

    public static void setACCESSLOG(Boolean ACCESSLOG) {
        LinkLogSwitch.ACCESSLOG = ACCESSLOG;
    }


}
