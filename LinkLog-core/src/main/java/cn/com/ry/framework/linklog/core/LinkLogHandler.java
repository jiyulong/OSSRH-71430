package cn.com.ry.framework.linklog.core;

import cn.com.ry.framework.linklog.constant.LinkLogConstants;
import cn.com.ry.framework.linklog.context.LinkLogContext;
import cn.com.ry.framework.linklog.context.LinkLogLabelGenerator;
import cn.com.ry.framework.linklog.id.LinkLogIdGeneratorLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * LinkLog的RPC处理逻辑的封装类
 *
 */
public class LinkLogHandler {

    protected static final Logger log = LoggerFactory.getLogger(LinkLogHandler.class);

    public void processProviderSide(LinkLogLabel labelBean) {
        if (labelBean.getPreApp() == null || labelBean.getPreApp().equals("")) {
            labelBean.setPreApp(LinkLogConstants.UNKNOWN);
        }
        LinkLogContext.putPreApp(labelBean.getPreApp());

        if (labelBean.getPreHost() == null || labelBean.getPreHost().equals("")) {
            labelBean.setPreHost(LinkLogConstants.UNKNOWN);
        }
        LinkLogContext.putPreHost(labelBean.getPreHost());

        if (labelBean.getPreIp() == null || labelBean.getPreIp().equals("")) {
            labelBean.setPreIp(LinkLogConstants.UNKNOWN);
        }
        LinkLogContext.putPreIp(labelBean.getPreIp());

        //如果从隐式传参里没有获取到，则重新生成一个traceId
        if (labelBean.getTraceId() == null || labelBean.getTraceId().equals("")) {
            labelBean.setTraceId(LinkLogIdGeneratorLoader.getIdGenerator().generateTraceId());
        }

        LinkLogContext.putSpanId(labelBean.getSpanId());

        LinkLogContext.putTraceId(labelBean.getTraceId());

        //生成日志标签
        String linkLogLabel = LinkLogLabelGenerator.generateLinkLogLabel(labelBean.getPreApp(),
                labelBean.getPreHost(),
                labelBean.getPreIp(),
                labelBean.getTraceId(),
                LinkLogContext.getSpanId());

        //如果有MDC，则往MDC中放入日志标签
        if (LinkLogContext.hasLinkLogMDC()) {
            MDC.put(LinkLogConstants.MDC_KEY, linkLogLabel);
        }
    }

    public void cleanThreadLocal() {
        //移除ThreadLocal里的数据
        LinkLogContext.removePreApp();
        LinkLogContext.removePreHost();
        LinkLogContext.removePreIp();
        LinkLogContext.removeTraceId();
        LinkLogContext.removeSpanId();
        if (LinkLogContext.hasLinkLogMDC()) {
            MDC.remove(LinkLogConstants.MDC_KEY);
        }
    }
}
