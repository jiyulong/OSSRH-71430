package cn.com.ry.framework.linklog.logging.logback;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import org.slf4j.LoggerFactory;

public class LogbackLinkLogSystem {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(LogbackLinkLogSystem.class);

    /**
     * 设置日志级别
     *
     * @param loggerName 日志名称
     * @param level 日志等级
     */
    public  static void setLevel(String loggerName, String level) {
        try {
            LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
            Logger vLogger = loggerContext.getLogger(loggerName);
            if (vLogger != null) {
                vLogger.setLevel(Level.toLevel(level));
            }
        } catch (Exception e) {
            logger.error("日志级别修改失败");
        }

    }
}
