package cn.com.ry.framework.linklog.fegin.common;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.support.AbstractApplicationContext;

public class FeginPluginCommon {

    public static void setFeginPlugin(AbstractApplicationContext applicationContext) {
        if (applicationContext != null) {
            //创建fegin拦截器
            createLinkLogFeignFilter(applicationContext);
        }
    }


    public static void createLinkLogFeignFilter(AbstractApplicationContext applicationContext) {
        try {
            applicationContext.getBean("linkLogFeignFilter");
        } catch (Exception e) {
            try {
                //没有该bean就创建一个
                BeanDefinitionBuilder linkLogFeignFilterBuilder = BeanDefinitionBuilder.genericBeanDefinition("com.ymdd.dorado.adapter.linklog.fegin.filter.LinkLogFeignFilter");
                DefaultListableBeanFactory linkLogFeignFilterBeanFacotry = (DefaultListableBeanFactory) applicationContext.getBeanFactory();
                linkLogFeignFilterBeanFacotry.registerBeanDefinition("linkLogFeignFilter", linkLogFeignFilterBuilder.getBeanDefinition());
            } catch (Exception e1) {
            }

        }
    }

}
