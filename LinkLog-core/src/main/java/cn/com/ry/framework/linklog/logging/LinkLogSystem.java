package cn.com.ry.framework.linklog.logging;


import cn.com.ry.framework.linklog.constant.LinkLogConstants;
import cn.com.ry.framework.linklog.logging.log4j2.Log4j2LinkLogSystem;
import cn.com.ry.framework.linklog.logging.logback.LogbackLinkLogSystem;
import cn.com.ry.framework.linklog.util.LinkLogTypeUtil;

public class LinkLogSystem {

    public static void setLevel(String loggerName, String level) {
        //判断日志系统类型
        if (loggerName != null && !loggerName.trim().equals("") && checkLevel(level)) {
            if (LinkLogTypeUtil.checkLinkLogType(LinkLogConstants.LINKLOG_LOGBACK)) {
                LogbackLinkLogSystem.setLevel(loggerName, level);
            }
            if (LinkLogTypeUtil.checkLinkLogType(LinkLogConstants.LINKLOG_LOG4j2)) {
                Log4j2LinkLogSystem.setLevel(loggerName, level);
            }
        }

    }

    public static boolean checkLevel(String level) {
        String[] levels = {"OFF", "FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE", "ALL"};
        if (level != null && !level.trim().equals("")) {
            for (String tLevel : levels) {
                if (tLevel.equalsIgnoreCase(level)) {
                    return true;
                }
            }
        }
        return false;
    }

}
