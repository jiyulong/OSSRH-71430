package cn.com.ry.framework.linklog.util;


import java.util.HashSet;
import java.util.Set;

/**
 * 判断系统类型
 */
public class LinkLogTypeUtil {
    private static Set linkLogTypeSet = new HashSet();

    public static void setLinkLogType(String linkLogType) {
        linkLogTypeSet.add(linkLogType);
    }


    public static boolean checkLinkLogType(String linkLogType) {
        return linkLogTypeSet.contains(linkLogType);
    }


}
