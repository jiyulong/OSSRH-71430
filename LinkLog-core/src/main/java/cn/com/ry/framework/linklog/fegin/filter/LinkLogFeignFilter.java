package cn.com.ry.framework.linklog.fegin.filter;


import cn.com.ry.framework.linklog.constant.LinkLogConstants;
import cn.com.ry.framework.linklog.context.LinkLogContext;
import cn.com.ry.framework.linklog.context.SpanIdGenerator;
import cn.com.ry.framework.linklog.util.HostInfo;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LinkLogFeignFilter implements RequestInterceptor {

    private static final Logger logger = LoggerFactory.getLogger(LinkLogFeignFilter.class);

    @Override
    public void apply(RequestTemplate requestTemplate) {
        String traceId = LinkLogContext.getTraceId();
        if (traceId != null && !traceId.equals("")) {
            requestTemplate.header(LinkLogConstants.LINKLOG_TRACE_KEY, traceId);
            requestTemplate.header(LinkLogConstants.LINKLOG_SPANID_KEY, SpanIdGenerator.generateNextSpanId());
            requestTemplate.header(LinkLogConstants.LINKLOG_PRE_APP_KEY, HostInfo.getAppName());
            requestTemplate.header(LinkLogConstants.LINKLOG_PRE_HOST_KEY, HostInfo.getHostName());
            requestTemplate.header(LinkLogConstants.LINKLOG_PRE_IP_KEY, HostInfo.getIp());
        } else {
            logger.debug("[LinkLog]本地threadLocal变量没有正确传递traceId,本次调用不传递traceId");
        }
    }
}
