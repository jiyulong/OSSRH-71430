package cn.com.ry.framework.linklog.util;

import cn.com.ry.framework.linklog.constant.LinkLogConstants;

public class SystemAdapterUtil {
    public static void checkSystemType() {
        //httpclient
        try {
            Class.forName("org.apache.http.impl.client.HttpClientBuilder");
            LinkLogTypeUtil.setLinkLogType(LinkLogConstants.LINKLOG_HTTPCLIENT);
        } catch (ClassNotFoundException e) {
        }

        //判断springboot
        try {
            Class.forName("org.springframework.web.servlet.config.annotation.WebMvcConfigurer");
            LinkLogTypeUtil.setLinkLogType(LinkLogConstants.LINKLOG_SPRINGBOOT);
        } catch (ClassNotFoundException e) {
        }
        //判断springMvc
        try {
            Class.forName("org.springframework.web.servlet.HandlerInterceptor");
            Class.forName("javax.servlet.http.HttpServletRequest");
            LinkLogTypeUtil.setLinkLogType(LinkLogConstants.LINKLOG_SPRINGMVC);
        } catch (ClassNotFoundException e) {
        }

        try {
            //判断dubbo架
            Class.forName("com.alibaba.dubbo.rpc.Filter");
            LinkLogTypeUtil.setLinkLogType(LinkLogConstants.LINKLOG_DUBBO);
        } catch (ClassNotFoundException e) {
        }

        //判断fegin
        try {
            Class.forName("feign.RequestInterceptor");
            LinkLogTypeUtil.setLinkLogType(LinkLogConstants.LINKLOG_FEGIN);
        } catch (ClassNotFoundException e) {
        }

        //判断gateway
        try {
            Class.forName("org.springframework.cloud.gateway.filter.GlobalFilter");
            LinkLogTypeUtil.setLinkLogType(LinkLogConstants.LINKLOG_GATEWAY);
        } catch (ClassNotFoundException e) {
        }


        //判断disconf
        try {
            Class.forName("com.baidu.disconf.client.common.update.IDisconfUpdatePipeline");
            Class.forName("com.baidu.disconf.client.store.DisconfStoreProcessor");
            Class.forName("com.baidu.disconf.client.store.DisconfStoreProcessorFactory");
            LinkLogTypeUtil.setLinkLogType(LinkLogConstants.LINKLOG_DISCONF);
        } catch (ClassNotFoundException e) {
        }


        //判断logback
        try {
            Class.forName("ch.qos.logback.classic.Logger");
            LinkLogTypeUtil.setLinkLogType(LinkLogConstants.LINKLOG_LOGBACK);
        } catch (ClassNotFoundException e) {
        }

        //判断apollo
        try {
            Class.forName("com.ctrip.framework.apollo.model.ConfigChangeEvent");
            LinkLogTypeUtil.setLinkLogType(LinkLogConstants.LINKLOG_APOLLO);
        } catch (ClassNotFoundException e) {
        }
    }
}
