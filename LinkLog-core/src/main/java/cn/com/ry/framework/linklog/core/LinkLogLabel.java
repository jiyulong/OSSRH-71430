package cn.com.ry.framework.linklog.core;

/**
 * LinkLog的日志标签包装类
 */
public class LinkLogLabel {

    private String preApp;

    private String preHost;

    private String preIp;

    private String traceId;

    private String spanId;

    public LinkLogLabel() {
    }

    public LinkLogLabel(String preApp, String preHost, String preIp, String traceId, String spanId) {
        this.preApp = preApp;
        this.preHost = preHost;
        this.preIp = preIp;
        this.traceId = traceId;
        this.spanId = spanId;
    }

    public String getPreApp() {
        return preApp;
    }

    public void setPreApp(String preApp) {
        this.preApp = preApp;
    }

    public String getPreIp() {
        return preIp;
    }

    public void setPreIp(String preIp) {
        this.preIp = preIp;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getSpanId() {
        return spanId;
    }

    public void setSpanId(String spanId) {
        this.spanId = spanId;
    }

    public String getPreHost() {
        return preHost;
    }

    public void setPreHost(String preHost) {
        this.preHost = preHost;
    }
}
