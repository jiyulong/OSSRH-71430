package cn.com.ry.framework.linklog;

import cn.com.ry.framework.linklog.configcenter.apollo.LinkLogApolloLisenter;
import cn.com.ry.framework.linklog.configcenter.disconf.LinkLogDisconfLisenter;
import cn.com.ry.framework.linklog.constant.LinkLogConstants;
import cn.com.ry.framework.linklog.util.LinkLogTypeUtil;
import cn.com.ry.framework.linklog.util.SystemAdapterUtil;
import cn.com.ry.framework.linklog.web.LinkLogSpringMvcListener;
import org.slf4j.TtlMDCAdapter;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import java.util.Set;


public class LinkLogServletContainerInitializer implements ServletContainerInitializer {

    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) {
        //判断系统类型
        SystemAdapterUtil.checkSystemType();
        //覆写logbackMDC
        if (LinkLogTypeUtil.checkLinkLogType(LinkLogConstants.LINKLOG_LOGBACK)) {
            TtlMDCAdapter.getInstance();
        }
        if (LinkLogTypeUtil.checkLinkLogType(LinkLogConstants.LINKLOG_SPRINGMVC)) {
            LinkLogSpringMvcListener linkLogSpringMvcListener = new LinkLogSpringMvcListener();
            ctx.addListener(linkLogSpringMvcListener);
        }

        if (LinkLogTypeUtil.checkLinkLogType(LinkLogConstants.LINKLOG_DISCONF)) {
            LinkLogDisconfLisenter linkLogDisconfLisenter = new LinkLogDisconfLisenter();
            ctx.addListener(linkLogDisconfLisenter);
        }

        if (LinkLogTypeUtil.checkLinkLogType(LinkLogConstants.LINKLOG_APOLLO)) {
            LinkLogApolloLisenter linkLogApolloLisenter = new LinkLogApolloLisenter();
            ctx.addListener(linkLogApolloLisenter);
        }
    }

}

