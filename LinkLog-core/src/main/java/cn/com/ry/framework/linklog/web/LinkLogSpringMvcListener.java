package cn.com.ry.framework.linklog.web;

import cn.com.ry.framework.linklog.web.common.SpringMvcPluginCommon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class LinkLogSpringMvcListener implements ServletContextListener {
    private static final Logger logger = LoggerFactory.getLogger(LinkLogSpringMvcListener.class);


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        AbstractApplicationContext applicationContext = (AbstractApplicationContext) WebApplicationContextUtils.getWebApplicationContext(servletContextEvent.getServletContext());

        logger.info("[linkLog][开始]加载LinkLogSpringMvcPlugin");
        SpringMvcPluginCommon.setSpringMvcPlugin(applicationContext);
        logger.info("[linkLog][结束]加载LinkLogSpringMvcPlugin");

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }


}
