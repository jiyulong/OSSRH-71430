package cn.com.ry.framework.linklog;

import cn.com.ry.framework.linklog.configcenter.apollo.common.LinkLogApolloPluginCommon;
import cn.com.ry.framework.linklog.configcenter.disconf.common.LinkLogDisconfPluginCommon;
import cn.com.ry.framework.linklog.constant.LinkLogConstants;
import cn.com.ry.framework.linklog.util.LinkLogTypeUtil;
import cn.com.ry.framework.linklog.util.SystemAdapterUtil;
import cn.com.ry.framework.linklog.web.common.SpringMvcPluginCommon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.AbstractApplicationContext;

public class LinkLogInit implements ApplicationContextAware {

    private static final Logger logger = LoggerFactory.getLogger(LinkLogInit.class);

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        //判断系统类型
        SystemAdapterUtil.checkSystemType();

        if (LinkLogTypeUtil.checkLinkLogType(LinkLogConstants.LINKLOG_SPRINGMVC)) {
            logger.info("[linkLog][开始]加载LinkLogSpringMvcPlugin");
            SpringMvcPluginCommon.setSpringMvcPlugin((AbstractApplicationContext) applicationContext);
            logger.info("[linkLog][结束]加载LinkLogSpringMvcPlugin");
        }

        if (LinkLogTypeUtil.checkLinkLogType(LinkLogConstants.LINKLOG_DISCONF)) {
            logger.info("[linkLog][开始]加载LinkLogDisconfPlugin");
            LinkLogDisconfPluginCommon.setLinkLogDisconfPlugin((AbstractApplicationContext) applicationContext);
            logger.info("[linkLog][开始]加载LinkLogDisconfPlugin");
        }

        if (LinkLogTypeUtil.checkLinkLogType(LinkLogConstants.LINKLOG_APOLLO)) {
            logger.info("[linkLog][开始]加载LinkLogApolloPlugin");
            LinkLogApolloPluginCommon.setLinkLogApolloPlugin((AbstractApplicationContext) applicationContext);
            logger.info("[linkLog][开始]加载LinkLogApolloPlugin");
        }
    }
}
