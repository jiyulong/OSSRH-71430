package cn.com.ry.framework.linklog.mdc;

import cn.com.ry.framework.linklog.constant.LinkLogConstants;
import cn.com.ry.framework.linklog.util.LinkLogTypeUtil;
import cn.com.ry.framework.linklog.util.SystemAdapterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.TtlMDCAdapter;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;


public class LinkLogMDCAutoConfiguration implements ApplicationContextInitializer {
    private static final Logger logger = LoggerFactory.getLogger(LinkLogMDCAutoConfiguration.class);

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        SystemAdapterUtil.checkSystemType();
        //覆写logbackMDC
        if (LinkLogTypeUtil.checkLinkLogType(LinkLogConstants.LINKLOG_LOGBACK)) {
            TtlMDCAdapter.getInstance();
        }
    }
}
