package cn.com.ry.framework.linklog.starter;

import cn.com.ry.framework.linklog.gateway.filter.LinkLogGatewayFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
 *gateway自动装配
 */
@Configuration
@ConditionalOnClass(name = {"org.springframework.cloud.gateway.filter.GlobalFilter"})
public class LinkLogGatewayAutoConfiguration {

    @Bean
    public LinkLogGatewayFilter linkLogGatewayFilter() {
        return new LinkLogGatewayFilter();
    }
}
