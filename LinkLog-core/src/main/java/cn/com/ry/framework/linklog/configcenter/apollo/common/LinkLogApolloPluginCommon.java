package cn.com.ry.framework.linklog.configcenter.apollo.common;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.support.AbstractApplicationContext;

public class LinkLogApolloPluginCommon {

    public static void setLinkLogApolloPlugin(AbstractApplicationContext applicationContext) {
        if (applicationContext != null) {
            createLinkLogApollo(applicationContext);
        }
    }


    public static void createLinkLogApollo(AbstractApplicationContext applicationContext) {
        try {
            applicationContext.getBean("linkLogApollo");
        } catch (Exception e) {
            try {
                BeanDefinitionBuilder linkLogApolloBuilder = BeanDefinitionBuilder.genericBeanDefinition("com.ymdd.dorado.adapter.linklog.configcenter.apollo.LinkLogApollo");
                DefaultListableBeanFactory linkLogApolloBeanFacotry = (DefaultListableBeanFactory) applicationContext.getBeanFactory();
                linkLogApolloBeanFacotry.registerBeanDefinition("linkLogApollo", linkLogApolloBuilder.getBeanDefinition());
            } catch (Exception e1) {

            }

        }
    }

}
