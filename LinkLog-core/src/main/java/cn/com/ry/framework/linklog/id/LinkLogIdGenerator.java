package cn.com.ry.framework.linklog.id;

public interface LinkLogIdGenerator {

    String generateTraceId();
}
