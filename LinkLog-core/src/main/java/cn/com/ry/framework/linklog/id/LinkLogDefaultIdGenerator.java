package cn.com.ry.framework.linklog.id;

import cn.com.ry.framework.linklog.id.snowflake.UniqueIdGenerator;

public class LinkLogDefaultIdGenerator implements LinkLogIdGenerator {
    @Override
    public String generateTraceId() {
        return UniqueIdGenerator.generateStringId();
    }
}
