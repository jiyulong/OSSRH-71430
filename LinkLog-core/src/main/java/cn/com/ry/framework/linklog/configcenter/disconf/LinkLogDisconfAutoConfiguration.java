package cn.com.ry.framework.linklog.configcenter.disconf;

import cn.com.ry.framework.linklog.configcenter.disconf.common.LinkLogDisconfPluginCommon;
import cn.com.ry.framework.linklog.constant.LinkLogConstants;
import cn.com.ry.framework.linklog.util.LinkLogTypeUtil;
import cn.com.ry.framework.linklog.util.SystemAdapterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;


public class LinkLogDisconfAutoConfiguration implements ApplicationContextInitializer {
    private static final Logger logger = LoggerFactory.getLogger(LinkLogDisconfAutoConfiguration.class);

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        SystemAdapterUtil.checkSystemType();
        //自动加载disconf
        if (LinkLogTypeUtil.checkLinkLogType(LinkLogConstants.LINKLOG_DISCONF)) {
            logger.info("[linkLog][开始]加载LinkLogDisconfPlugin");
            LinkLogDisconfPluginCommon.setLinkLogDisconfPlugin((AbstractApplicationContext) applicationContext);
            logger.info("[linkLog][结束]加载LinkLogDisconfPlugin");
        }

    }
}
