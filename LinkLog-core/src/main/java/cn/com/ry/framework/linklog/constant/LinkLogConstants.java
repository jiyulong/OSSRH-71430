package cn.com.ry.framework.linklog.constant;

/**
 * 静态变量类
 */
public class LinkLogConstants {

    public static final String LINKLOG_TRACE_KEY = "linkLogTraceid";

    public static final String LINKLOG_SPANID_KEY = "linkLogSpanid";

    public static final String LINKLOG_PRE_APP_KEY = "linkLogPreApp";

    public static final String LINKLOG_PRE_HOST_KEY = "linkLogPreHost";

    public static final String LINKLOG_PRE_IP_KEY = "linkLogPreIp";

    public static final String UNKNOWN = "NONE";

    public static final String MDC_KEY = "linkLog";

    public static final String LINKLOG_DUBBO = "dubbo";

    public static final String LINKLOG_HTTPCLIENT = "httpclient";

    public static final String LINKLOG_FEGIN = "fegin";

    public static final String LINKLOG_GATEWAY = "gateway";

    public static final String LINKLOG_SPRINGBOOT = "springboot";

    public static final String LINKLOG_SPRINGMVC = "springMVC";

    public static final String LINKLOG_LOGBACK = "logback";

    public static final String LINKLOG_LOG4j2 = "log4j2";

    public static final String LINKLOG_DISCONF = "disconf";

    public static final String LINKLOG_APOLLO = "apollo";

    public static final String LINKLOG_SUFFIX = ".properties";

    public static final String LINKLOG_LOG_CLIENT = "client";
    //服务端标识
    public static final String LINKLOG_LOG_SERVER = "server";
    //如果请求内容超过1M则不打印日志
    public static final Long maxContentLength = 128 * 1024L;
}
