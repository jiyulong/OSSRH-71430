package cn.com.ry.framework.linklog.gateway.filter;

import cn.com.ry.framework.linklog.gateway.common.LinkLogGatewayCommon;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * gateway 的全局拦截器
 */
public class LinkLogGatewayFilter implements GlobalFilter, Ordered {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        return chain.filter(LinkLogGatewayCommon.loadInstance().preHandle(exchange))
                .doFinally(signalType -> LinkLogGatewayCommon.loadInstance().cleanThreadLocal());
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
