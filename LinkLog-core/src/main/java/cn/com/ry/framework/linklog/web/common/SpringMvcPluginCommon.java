package cn.com.ry.framework.linklog.web.common;

import cn.com.ry.framework.linklog.web.interceptor.LinkLogSpringMvcInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.support.AbstractApplicationContext;

public class SpringMvcPluginCommon {
    private static final Logger logger = LoggerFactory.getLogger(SpringMvcPluginCommon.class);

    public static void setSpringMvcPlugin(AbstractApplicationContext applicationContext) {
        if (applicationContext != null) {
            createLinkLogSpringMvcInterceptor(applicationContext);
        }
    }


    public static void createLinkLogSpringMvcInterceptor(AbstractApplicationContext applicationContext) {
        try {
            applicationContext.getBean("linkLogSpringMvcConfigurer");
        } catch (Exception e) {
            try {
                //没有该bean就创建一个
                BeanDefinitionBuilder mappedInterceptorBuilder = BeanDefinitionBuilder.genericBeanDefinition("org.springframework.web.servlet.handler.MappedInterceptor");
                mappedInterceptorBuilder.addConstructorArgValue("/**");
                mappedInterceptorBuilder.addConstructorArgValue(new LinkLogSpringMvcInterceptor());
                DefaultListableBeanFactory mappedInterceptorBeanFacotry = (DefaultListableBeanFactory) applicationContext.getBeanFactory();
                mappedInterceptorBeanFacotry.registerBeanDefinition("linkLogSpringMvcConfigurer", mappedInterceptorBuilder.getBeanDefinition());
            } catch (Exception e1) {

            }

        }
    }
}

