package cn.com.ry.framework.linklog.jersey;


import cn.com.ry.framework.linklog.constant.LinkLogConstants;
import cn.com.ry.framework.linklog.context.LinkLogContext;
import cn.com.ry.framework.linklog.context.SpanIdGenerator;
import cn.com.ry.framework.linklog.core.LinkLogHandler;
import cn.com.ry.framework.linklog.util.HostInfo;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

public class LinkLogJerseyFilter extends LinkLogHandler implements ClientRequestFilter {

    @Override
    public void filter(ClientRequestContext context) {
        try {
            if (context != null && context.getHeaders() != null) {
                String traceId = LinkLogContext.getTraceId();
                if (traceId != null && traceId != "") {
                    String appName = HostInfo.getAppName();
                    String ip = HostInfo.getIp();
                    String hostName = HostInfo.getHostName();
                    String spanId = SpanIdGenerator.generateNextSpanId();
                    context.getHeaders().add(LinkLogConstants.LINKLOG_PRE_APP_KEY, appName);
                    context.getHeaders().add(LinkLogConstants.LINKLOG_PRE_HOST_KEY, hostName);
                    context.getHeaders().add(LinkLogConstants.LINKLOG_PRE_IP_KEY, ip);
                    context.getHeaders().add(LinkLogConstants.LINKLOG_TRACE_KEY, traceId);
                    context.getHeaders().add(LinkLogConstants.LINKLOG_SPANID_KEY, spanId);
                } else {
                    //2.如果没有就不处理
                    log.debug("[LinkLog]本地threadLocal变量没有正确传递traceId,本次调用不传递traceId");
                }
            }
        } catch (Exception e) {
            log.error("Jersey Filter执行失败", e);
        }

    }
}
