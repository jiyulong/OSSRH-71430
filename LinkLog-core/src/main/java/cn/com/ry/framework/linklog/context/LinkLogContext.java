package cn.com.ry.framework.linklog.context;


import com.alibaba.ttl.TransmittableThreadLocal;

/**
 * LinkLog上下文
 */
public class LinkLogContext {

    private static boolean enableInvokeTimePrint = false;

    private static boolean hasLinkLogMDC = true;

    private static TransmittableThreadLocal<String> traceIdLinkLog = new TransmittableThreadLocal<>();

    private static TransmittableThreadLocal<String> preAppLinkLog = new TransmittableThreadLocal<>();

    private static TransmittableThreadLocal<String> preHostLinkLog = new TransmittableThreadLocal<>();

    private static TransmittableThreadLocal<String> preIpLinkLog = new TransmittableThreadLocal<>();

    public static void putTraceId(String traceId) {
        traceIdLinkLog.set(traceId);
    }

    public static String getTraceId() {
        return traceIdLinkLog.get();
    }

    public static void removeTraceId() {
        traceIdLinkLog.remove();
    }

    public static void putSpanId(String spanId) {
        SpanIdGenerator.putSpanId(spanId);
    }

    public static String getSpanId() {
        return SpanIdGenerator.getSpanId();
    }

    public static void removeSpanId() {
        SpanIdGenerator.removeSpanId();
    }

    public static String getPreApp() {
        return preAppLinkLog.get();
    }

    public static void putPreApp(String preApp) {
        preAppLinkLog.set(preApp);
    }

    public static void removePreApp() {
        preAppLinkLog.remove();
    }

    public static String getPreHost() {
        return preHostLinkLog.get();
    }

    public static void putPreHost(String preHost) {
        preHostLinkLog.set(preHost);
    }

    public static void removePreHost() {
        preHostLinkLog.remove();
    }

    public static String getPreIp() {
        return preIpLinkLog.get();
    }

    public static void putPreIp(String preIp) {
        preIpLinkLog.set(preIp);
    }

    public static void removePreIp() {
        preIpLinkLog.remove();
    }

    public static boolean hasLinkLogMDC() {
        return hasLinkLogMDC;
    }

    public static void sethasLinkLogMDC(boolean hasLinkLogMDC) {
        LinkLogContext.hasLinkLogMDC = hasLinkLogMDC;
    }

    public static boolean enableInvokeTimePrint() {
        return enableInvokeTimePrint;
    }

    public static void setEnableInvokeTimePrint(boolean enableInvokeTimePrint) {
        LinkLogContext.enableInvokeTimePrint = enableInvokeTimePrint;
    }
    public static String toMdc() {
        //生成日志标签
        String linkLogLabel = LinkLogLabelGenerator.generateLinkLogLabel(LinkLogContext.getPreApp(),
                LinkLogContext.getPreHost(),
                LinkLogContext.getPreIp(),
                LinkLogContext.getTraceId(),
                LinkLogContext.getSpanId());
        return linkLogLabel;
    }
}
