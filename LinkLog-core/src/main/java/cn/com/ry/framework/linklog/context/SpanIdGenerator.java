package cn.com.ry.framework.linklog.context;

import com.alibaba.ttl.TransmittableThreadLocal;

/**
 * spanId生成器
 *
 */
public class SpanIdGenerator {

    private static TransmittableThreadLocal<String> currentSpanId = new TransmittableThreadLocal<>();

    private static TransmittableThreadLocal<Integer> spanIndex = new TransmittableThreadLocal<>();

    private static String INITIAL_VALUE = "0";

    public static void putSpanId(String spanId) {
        if (spanId == null || spanId.equals("")) {
            spanId = INITIAL_VALUE;
        }
        currentSpanId.set(spanId);
        spanIndex.set(Integer.valueOf(INITIAL_VALUE));
    }

    public static String getSpanId() {
        return currentSpanId.get();
    }

    public static void removeSpanId() {
        currentSpanId.remove();
    }

    public static String generateNextSpanId() {
        //只在同一个request请求里进行线程安全操作
        synchronized (LinkLogContext.getTraceId()) {
            String currentSpanId = LinkLogContext.getSpanId();
            spanIndex.set(spanIndex.get() + 1);
            String nextSpanId = currentSpanId + "." + spanIndex.get();
            return nextSpanId;
        }
    }
}
